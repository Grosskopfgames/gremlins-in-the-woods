shader_type canvas_item;
uniform sampler2D ground:hint_black;
uniform sampler2D grass:hint_black;
uniform sampler2D noise:hint_black;

void fragment() {
	vec4 groundcolor=texture(ground,UV);
	vec4 grasscolor=texture(grass,UV);
	float amount=texture(noise,UV*0.1).r;
	COLOR = groundcolor*amount+grasscolor*(1.0-amount);
	//COLOR=texture(noise,UV*vec2(1,1));
}
extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var house=preload("res://House.tscn")
var tree=preload("res://Tree.tscn")
var ground=preload("res://Ground.tscn")
var tiles=[Vector2(0,0)]
var currenttile=Vector2(0,0)
var noise
var houses=[]
var trees=[]
var seednum=0

var tilesize
var minutes=0

var raining=false
# Called when the node enters the scene tree for the first time.
func _ready():
	tilesize=get_viewport_rect().size*0.6
	noise = OpenSimplexNoise.new()
	# Configure
	seednum=randi()
	noise.seed = seednum
	noise.octaves = 4
	noise.period = 200.0
	noise.persistence = 0.8
	var treesplanted=0
	var housesset=0
	randomize()
	var actground=ground.instance()
	actground.scale=Vector2(tilesize.x/1920,tilesize.y/1080)
	$Groundroot.add_child(actground)
	generate_things(-1,-1)
	generate_things(-1,0)
	generate_things(-1,1)
	generate_things(0,-1)
	generate_things(0,1)
	generate_things(1,-1)
	generate_things(1,0)
	generate_things(1,1)
	pass # Replace with function body.

func _process(delta):
	minutes+=delta/60
	if(global.weather=="raining" and !raining):
		$Camera2D/Particles2D.visible=true
		modulate=Color(0.6,0.6,0.7)
		raining=true
	if(global.weather=="sunny" and raining):
		$Camera2D/Particles2D.visible=false
		modulate=Color(1,1,1)
		raining=false

func generate_things(var h,var v):
	if(tiles.find(Vector2(h,v))!=-1):
		return
	var actground=ground.instance()
	var noisetex= ImageTexture.new()
	var imgnoise=Image.new()
	imgnoise.create(int(tilesize.x/100),int(tilesize.y/100),false,Image.FORMAT_RGB8)
	imgnoise.lock()
	for i in range(int(tilesize.x/100)):
		for j in range(int(tilesize.y/100)):
			var globalpos=Vector2(h*tilesize.x-(tilesize.x/2)+i*100,v*tilesize.y-(tilesize.y/2)+j*100)
			var val=(noise.get_noise_2d(globalpos.x,globalpos.y)+1)/2
			var custcolor=Color(val,val,val)
			if(val>=0 and val<=1):
				imgnoise.set_pixel(i,j,custcolor)
	imgnoise.unlock()
	imgnoise.resize(1920/10,1080/10,Image.INTERPOLATE_CUBIC)
	noisetex.create_from_image(imgnoise)
	actground.get_node("Sprite").material.set_shader_param("noise",noisetex)
	$Groundroot.add_child(actground)
	actground.position=Vector2(h*tilesize.x,v*tilesize.y)
	actground.scale=Vector2(tilesize.x/1920,tilesize.y/1080)
	for i in range(int(tilesize.x/10)):
		for j in range(int(tilesize.y/10)):
			var globalpos=Vector2(h*tilesize.x-(tilesize.x/2)+i*10,v*tilesize.y-(tilesize.y/2)+j*10)
			var val=noise.get_noise_2d(globalpos.x,globalpos.y)
			if(val/(500+100*(abs(h)+abs(v)))>randf()):
				var neighbourtooclose=false
				for house in houses:
					if(abs(globalpos.x-house.x)<36*2 and abs(globalpos.y-house.y)<36*2):
						neighbourtooclose=true
						print("too close to a house")
						break
				for tree in trees:
					if(abs(globalpos.x-tree.x)<46 and abs(globalpos.y-tree.y)<46):
						print("too close to a tree")
						neighbourtooclose=true
						break
				if(!neighbourtooclose):
					houses.append(globalpos)
					var acthouse=house.instance()
					acthouse.player=$Char
					$Houseroot.add_child(acthouse)
					acthouse.position=globalpos
			if(-val/(1500/(500+100*(abs(h)+abs(v))))>randf()):
				var actualpos=Vector2(globalpos.x+randf()*5-2.5,globalpos.y+randf()*5-2.5)
				var neighbourtooclose=false
				for house in houses:
					if(abs(globalpos.x-house.x)<46 and abs(globalpos.y-house.y)<46):
						neighbourtooclose=true
						break
				for tree in trees:
					if(globalpos.distance_to(tree)<20):
						neighbourtooclose=true
						
						break
				if(!neighbourtooclose):
					trees.append(globalpos)
					var acttree=tree.instance()
					$Treeroot.add_child(acttree)
					acttree.position=actualpos
	tiles.append(Vector2(h,v))

func gameover():
	self.modulate=Color(1,1,1)
	$Camera2D/Control/Gameover.visible=true
	$Camera2D/Control/Gameover/Label2.text="Survived"+str(int(minutes))+"Minutes"
	self.pause_mode=Node.PAUSE_MODE_STOP
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

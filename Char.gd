extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var itemdispblank=preload("res://Item.tscn")
var craftdispblank=preload("res://Craftrecipe.tscn")
var itemImages={}

var speed=Vector2(0,0)
var maxspeed=200
var food = 10
var maxfood = 10
var drink = 10
var maxdrink=10
var ui
var moving=false
var items={}
var itemdisp={}
var craftdisp={}
var fireplace
var watercollector
var fireplaceconstruct=preload("res://Fireplace.tscn")
var watercollectorconstruct=preload("res://Watercollector.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var rawmeat=Image.new()
	rawmeat.load("res://RawMeat.png")
	itemImages["RawMeat"]=rawmeat
	var meat=Image.new()
	meat.load("res://Meat.png")
	itemImages["Meat"]=meat
	var drink=Image.new()
	drink.load("res://Drink.png")
	itemImages["Drink"]=drink
	var wood=Image.new()
	wood.load("res://Wood.png")
	itemImages["Wood"]=wood
	var brick=Image.new()
	brick.load("res://Brick.png")
	itemImages["Brick"]=brick
	var shovel=Image.new()
	shovel.load("res://Shovel.png")
	itemImages["Shovel"]=shovel
	fireplace=Image.new()
	fireplace.load("res://campfire.png")
	watercollector=Image.new()
	watercollector.load("res://Watercollector.png")
	ui=get_parent().get_node("Camera2D/Control")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_and_collide(delta*speed*maxspeed)
	food -=delta*0.1
	drink -= delta*0.2
	ui.get_node("ProgressBar").value=10*food
	ui.get_node("ProgressBar2").value=10*drink
	if(!moving and $Character.texture.fps!=0):
		$Character.texture.fps=0
	if(moving and $Character.texture.fps==0):
		$Character.texture.fps=20
	if(food<-10 or drink<-10):
		get_parent().gameover()
	pass

func _input(event):
	if event.is_action_pressed("ui_down"):
		speed.y=1
	elif event.is_action_pressed("ui_up"):
		speed.y=-1
	if( event.is_action_released("ui_up") or event.is_action_released("ui_down")):
		speed.y=0
	if event.is_action_pressed("ui_left"):
		speed.x=-1
	elif event.is_action_pressed("ui_right"):
		speed.x=1
	if( event.is_action_released("ui_left") or event.is_action_released("ui_right")):
		speed.x=0
	moving=speed!=Vector2(0,0)

func updateitems():
	for item in items.keys():
		if !itemdisp.has(item):
			var thisItemDisp=itemdispblank.instance()
			thisItemDisp.setimg(itemImages[item])
			thisItemDisp.get_node("Label").text=str(items[item])
			if(item=="RawMeat"):
				thisItemDisp.type="rawmeat"
			if(item=="Meat"):
				thisItemDisp.type="goodfood"
			if(item=="Drink"):
				thisItemDisp.type="drink"
			thisItemDisp.character=self
			ui.get_node("VBoxContainer").add_child(thisItemDisp)
			itemdisp[item]=thisItemDisp
		else:
			itemdisp[item].get_node("Label").text=str(items[item])
			if(item=="Wood"):
				if(items[item]>=5 and !craftdisp.has("Fireplace")):
					var thisCraftDisp=craftdispblank.instance()
					thisCraftDisp.setimg(fireplace)
					thisCraftDisp.type="fireplace"
					thisCraftDisp.character=self
					thisCraftDisp.additems({"Wood":5})
					craftdisp["Fireplace"]=thisCraftDisp
					ui.get_node("HBoxContainer").add_child(thisCraftDisp)
			if(item=="Brick" and items["Brick"]>=15 and !craftdisp.has("Watercollector")):
				var thisCraftDisp=craftdispblank.instance()
				thisCraftDisp.setimg(watercollector)
				thisCraftDisp.type="watercollector"
				thisCraftDisp.character=self
				thisCraftDisp.additems({"Brick":15})
				craftdisp["Watercollector"]=thisCraftDisp
				ui.get_node("HBoxContainer").add_child(thisCraftDisp)
					
func reduceitem(var name,var amount):
	if(items[name]>=amount):
		items[name]-=amount
	itemdisp[name].get_node("Label").text=str(items[name])
	if(items[name]==0):
		items.erase(name)
		ui.get_node("VBoxContainer").remove_child(itemdisp[name])
		itemdisp.erase(name)
	if(name=="Wood" and craftdisp.has("Fireplace") and (!items.has("Wood") or items["Wood"]<5)):
		ui.get_node("HBoxContainer").remove_child(craftdisp["Fireplace"])
		craftdisp.erase("Fireplace")
	if(name=="Brick" and craftdisp.has("Watercollector") and (!items.has("Brick") or items["Brick"]<15)):
		ui.get_node("HBoxContainer").remove_child(craftdisp["Watercollector"])
		craftdisp.erase("Watercollector")
		
	pass
func spawnfireplace():
	reduceitem("Wood",5)
	var buildplatform=get_parent().get_node("Built")
	var thisfire=fireplaceconstruct.instance()
	thisfire.position=position
	thisfire.player=self
	buildplatform.add_child(thisfire)
func spawnwatercollector():
	reduceitem("Brick",15)
	var buildplatform=get_parent().get_node("Built")
	var thiswatercollector=watercollectorconstruct.instance()
	thiswatercollector.position=position
	thiswatercollector.player=self
	buildplatform.add_child(thiswatercollector)
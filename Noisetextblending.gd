extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var gras=preload("res://Gras.png")
var ground=preload("res://Schlamm.png")


# Called when the node enters the scene tree for the first time.
func _ready():
	var noise = ImageTexture.new()
	var img= Image.new()
	img.create(2, 2, false, Image.FORMAT_RGB8)
	img.lock()
	img.set_pixel(0,0,Color(0,0,0))
	img.set_pixel(0,1,Color(1,0,0))
	img.set_pixel(1,0,Color(0,1,0))
	img.set_pixel(1,1,Color(0,0,1))
	#img.unlock()
	img.resize(gras.get_width(),gras.get_height(),Image.INTERPOLATE_CUBIC)
	noise.create_from_image(img)
	$Sprite2.texture=noise
	
	$Sprite.material.set_shader_param("ground",ground)
	$Sprite.material.set_shader_param("gras",gras)
	$Sprite.material.set_shader_param("noise",noise)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

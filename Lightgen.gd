extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var amountLights=16

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(amountLights):
		var angle=i*360/amountLights
		var positionLight=Vector2(cos(angle)*100,sin(angle)*100)
		var actlight=Light2D.new()
		actlight.texture=$Light2D.texture
		actlight.texture_scale=4
		actlight.energy=0.5
		actlight.shadow_enabled=true
		actlight.shadow_filter=Light2D.SHADOW_FILTER_PCF7
		actlight.shadow_filter_smooth=4
		actlight.shadow_gradient_length=30
		actlight.position=positionLight
		self.add_child(actlight)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

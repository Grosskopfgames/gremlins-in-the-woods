extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var water=0
var maxwater=10
var player
var fulltex
var emptytex

# Called when the node enters the scene tree for the first time.
func _ready():
	fulltex=ImageTexture.new()
	var fullimg=Image.new()
	fullimg.load("res://Watercollector_full.png")
	fulltex.create_from_image(fullimg)
	emptytex=ImageTexture.new()
	var emptyimg=Image.new()
	emptyimg.load("res://Watercollector.png")
	emptytex.create_from_image(emptyimg)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(global.weather=="raining"):
		water=min(water+delta*0.1,maxwater)
		if(water>5 and $Sprite.texture!=fulltex):
			$Sprite.texture=fulltex
	if(overlaps_body(player)):
		if(Input.is_action_pressed("ui_select")):
			if(player.items.has("Drink")):
				player.items["Drink"]+=water
			else:
				player.items["Drink"]=water
			player.updateitems()
			water==0
			if($Sprite.texture!=emptytex):
				$Sprite.texture=emptytex
			player.updateitems()
	pass

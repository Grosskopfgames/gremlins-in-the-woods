extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var type
var character
var cost
var fontinf=preload("res://font.tres")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setimg(var img):
	var imgtexture=ImageTexture.new()
	imgtexture.create_from_image(img)
	$Texture.texture=imgtexture
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func additems(var dict):
	cost=dict
	for item in dict.keys():
		var imgtext=ImageTexture.new()
		imgtext.create_from_image(character.itemImages[item])
		var imgrect=TextureRect.new()
		imgrect.texture=imgtext
		imgrect.expand=true
		imgrect.rect_size=Vector2(36,36)
		imgrect.rect_min_size=Vector2(36,36)
		var label=Label.new()
		label.add_font_override("font",fontinf)
		label.text=str(dict[item])
		$HBoxContainer.add_child(imgrect)
		$HBoxContainer.add_child(label)
	pass


func _on_Panel_pressed():
	if type=="goodfood":
		character.food=min(character.food+1,character.maxfood)
		character.reduceitem("Meat",1)
	elif type=="drink":
		character.drink=min(character.drink+1,character.maxdrink)
		character.reduceitem("Drink",1)
	elif type=="rawfood":
		character.food=min(character.food+0.2,character.maxfood)
		character.reduceitem("RawFood",1)
	elif type=="fireplace" and abs(character.position.x)<character.get_parent().tilesize.x/2 and abs(character.position.y)<character.get_parent().tilesize.y/2:
		character.spawnfireplace()
	elif type=="watercollector" and abs(character.position.x)<character.get_parent().tilesize.x/2 and abs(character.position.y)<character.get_parent().tilesize.y/2:
		character.spawnwatercollector()
	self.focus_mode=Control.FOCUS_NONE
	pass # Replace with function body.

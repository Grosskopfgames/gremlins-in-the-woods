extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player
var meat=0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(overlaps_body(player)):
		if(Input.is_action_pressed("ui_select")):
			if(meat>0):
				if(player.items.has("Meat")):
					player.items["Meat"]+=meat
				else:
					player.items["Meat"]=meat
				meat=0
				player.updateitems()
			elif(player.items.has("Wood") and player.items.has("RawMeat")):
				if(player.items["RawMeat"]>10):
					player.reduceitem("Wood",1)
					player.reduceitem("RawMeat",10)
					$Timer.start()
					$Light2D.visible=true
					$Light2D2.visible=true
					$Light2D3.visible=true
					$Particles2D.visible=true
		$Sprite2.visible=true
	else:
		$Sprite2.visible=false
	pass


func _on_Timer_timeout():
	meat+=10
	$Light2D.visible=false
	$Light2D2.visible=false
	$Light2D3.visible=false
	$Particles2D.visible=false
	pass # Replace with function body.

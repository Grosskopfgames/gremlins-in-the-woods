extends Node

var timetochange=0
var weather="sunny"


func _ready():
	timetochange=randf()*120
	pass

func _process(delta):
	timetochange-=delta
	if(timetochange<=0):
		timetochange=randf()*120
		if(weather=="sunny"):
			weather="raining"
		else:
			weather="sunny"
	
extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var treenum=randi()%4
	if(treenum==3 and randf()<0.8):
		treenum=randi()%3
	$icon.region_rect=Rect2(200*treenum,0,200,400)
	$icon.scale.y=0.1+randf()/5
	$icon.scale.y=0.1+randf()/5
	$icon.offset.y=-150*($icon.scale.y/0.2)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends Camera2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	offset=-get_viewport_rect().size*0.5
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if(((get_parent().get_node("Char").global_position-global_position) /get_viewport_rect().size).x>0.2):
		global_position.x+=get_parent().tilesize.x
		$Control.rect_position=Vector2(0,0)
		get_parent().generate_things(get_parent().currenttile.x+2,get_parent().currenttile.y+1)
		get_parent().generate_things(get_parent().currenttile.x+2,get_parent().currenttile.y)
		get_parent().generate_things(get_parent().currenttile.x+2,get_parent().currenttile.y-1)
		get_parent().currenttile.x+=1
	if(((get_parent().get_node("Char").global_position-global_position) /get_viewport_rect().size).x<=-0.2):
		global_position.x+=-get_parent().tilesize.x
		$Control.rect_position=Vector2(0,0)
		get_parent().generate_things(get_parent().currenttile.x-2,get_parent().currenttile.y+1)
		get_parent().generate_things(get_parent().currenttile.x-2,get_parent().currenttile.y)
		get_parent().generate_things(get_parent().currenttile.x-2,get_parent().currenttile.y-1)
		get_parent().currenttile.x-=1
	if(((get_parent().get_node("Char").global_position-global_position) /get_viewport_rect().size).y>0.2):
		global_position.y+=get_parent().tilesize.y
		$Control.rect_position=Vector2(0,0)
		get_parent().generate_things(get_parent().currenttile.x-1,get_parent().currenttile.y+2)
		get_parent().generate_things(get_parent().currenttile.x,get_parent().currenttile.y+2)
		get_parent().generate_things(get_parent().currenttile.x+1,get_parent().currenttile.y+2)
		get_parent().currenttile.y+=1
	if(((get_parent().get_node("Char").global_position-global_position) /get_viewport_rect().size).y<=-0.2):
		global_position.y-=get_parent().tilesize.y
		$Control.rect_position=Vector2(0,0)
		get_parent().generate_things(get_parent().currenttile.x-1,get_parent().currenttile.y-2)
		get_parent().generate_things(get_parent().currenttile.x,get_parent().currenttile.y-2)
		get_parent().generate_things(get_parent().currenttile.x+1,get_parent().currenttile.y-2)
		get_parent().currenttile.y-=1
	pass

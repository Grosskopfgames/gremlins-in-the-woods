extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player
var item={}

# Called when the node enters the scene tree for the first time.
func _ready():
	if(randf()>0.6):
		item["RawMeat"]=(randi()%4)*10+10
	if(randf()>0.2):
		item["Wood"]=(randi()%4)+1
	if(randf()>0.7):
		item["Brick"]=(randi()%3)+1
	if(randf()>0.3):
		item["Drink"]=(randi()%4)*10+10
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(overlaps_body(player)):
		if(Input.is_action_pressed("ui_select")):
			for i in item.keys():
				if(player.items.has(i)):
					player.items[i]+=item[i]
				else:
					player.items[i]=item[i]
				item.erase(i)
			player.updateitems()
			$icon/Lightgen.visible=true
	pass
